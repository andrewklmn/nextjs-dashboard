'use client';

import { deleteInvoice } from '@/app/lib/actions';
import { ArrowPathIcon, TrashIcon } from '@heroicons/react/24/outline';
import { useState } from 'react';

export function DeleteInvoiceButton({ id }: { id: string }) {
  const [pending, setPending] = useState(false);

  const deleteInvoiceWithId = (async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    setPending(true);
    if (confirm('Are you sure you want to delete this invoice?')) {
      console.log('Deleting invoice with id:', id);
      const result = await deleteInvoice(id);
  
      if (result?.message) {
        alert(result.message);
      }
    }
    setPending(false);
  });

  console.log('==== DeleteInvoiceButton rendered');

  return (
    <form onSubmit={deleteInvoiceWithId}>
      <button
        className="rounded-md border p-2 hover:bg-gray-100"
        disabled={pending}
      >
        <span className="sr-only">Delete</span>
        {pending ? (
          <ArrowPathIcon className="w-5" />
        ) : (
          <TrashIcon className="w-5" />
        )}
      </button>
    </form>
  );
}
