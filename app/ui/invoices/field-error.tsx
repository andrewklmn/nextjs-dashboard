import { State, PropName } from '@/app/lib/actions';

export const FieldError = ({
  state,
  id,
  name,
}: {
  state: State;
  id: string;
  name: PropName;
}) => {
  const errors = state.errors?.[name] ?? [];

  return (
    <div id={id} aria-live="polite" aria-atomic="true">
      {errors.map((error: string, index) => (
        <p className="mt-2 text-sm text-red-500" key={`${error}-${index}`}>
          {error}
        </p>
      ))}
    </div>
  );
};
