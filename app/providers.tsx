'use client';

import { createContext, useContext, useState } from 'react';

type GlobalContextType = {
  isOpen: boolean;
  setIsOpen: (isOpen: boolean) => void;
};

const defaultGlobalContext: GlobalContextType = {
  isOpen: false,
  setIsOpen: () => {},
};

export const GlobalContext = createContext(defaultGlobalContext);

export function Providers({ children }: { children: React.ReactNode }) {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <GlobalContext.Provider value={{ isOpen, setIsOpen }}>
      {children}
    </GlobalContext.Provider>
  );
}
